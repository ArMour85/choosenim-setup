# choosenim-setup
This project repackages the [choosenim](https://github.com/nim-lang/choosenim) application to native setup to Windows and Chocolatey package manager.

The advantages contrary to the archive with script publish by choosenim is that native setup adds binaries path in the environment. Also, if you uninstall it, all created folders by application are removed and the environment is cleaned.

Isn't vetted by the Nim team but the choosenim binary is never modified and to be sure you can see the code.

Contributions are welcome.

## To be clear with you

This project isn't vetted by the Nim team but the choosenim binary is never modified and to be sure you can see the code.

## Where to download ?
You can download them [here](https://gitlab.com/EchoPouet/choosenim-setup/-/packages).

## Install with Chocolatey
Use the following command.

```console
choco install choosenim
```

## How it works ?
Every week, if a new release is published, the CI download it, generates several setups and publish them to package managers.

