#!/bin/bash

exePathToUpload=$(find $CI_PROJECT_DIR/upload-dir -iname *.exe)
exeToUpload=$(basename -- $exePathToUpload)
chocoPackagePathToUpload=$(find $CI_PROJECT_DIR/upload-dir -iname *.nupkg)
chocoPackageToUpload=$(basename -- $chocoPackagePathToUpload)

curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file $exePathToUpload "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/choosenim/$CHOOSENIM_VERSION/$exeToUpload"
curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file $chocoPackagePathToUpload "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/choosenim/$CHOOSENIM_VERSION/$chocoPackageToUpload"