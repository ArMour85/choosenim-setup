# Move binary
Copy-Item -Path .\upload-dir\choosenim-$env:CHOOSENIM_VERSION-amd64-setup.exe -Destination .\scripts\windows\choco\choosenim\tools

# Get license
Invoke-WebRequest -Uri https://raw.githubusercontent.com/nim-lang/choosenim/master/LICENSE -Out .\scripts\windows\choco\choosenim\tools\LICENSE.txt

# Change location
$currentLocation = Get-Location
Set-Location .\scripts\windows\choco\choosenim

# Get checksum
$checksum = Get-FileHash -Path .\tools\choosenim-$env:CHOOSENIM_VERSION-amd64-setup.exe

# Replace values in files
((Get-Content -path choosenim.nuspec -Raw) -replace '@VERSION@',$env:CHOOSENIM_VERSION) | Set-Content -Path choosenim.nuspec
((Get-Content -path .\tools\chocolateyinstall.ps1 -Raw) -replace '@VERSION@',$env:CHOOSENIM_VERSION) | Set-Content -Path .\tools\chocolateyinstall.ps1
((Get-Content -path .\tools\VERIFICATION.txt -Raw) -replace '@VERSION@',$env:CHOOSENIM_VERSION) | Set-Content -Path .\tools\VERIFICATION.txt
((Get-Content -path .\tools\chocolateyinstall.ps1 -Raw) -replace '@CHECKSUM@',$checksum.Hash) | Set-Content -Path .\tools\chocolateyinstall.ps1
((Get-Content -path .\tools\VERIFICATION.txt -Raw) -replace '@CHECKSUM@',$checksum.Hash) | Set-Content -Path .\tools\VERIFICATION.txt

# Run packaging
choco pack

# Publish
choco apikey --key $env:CHOCO_API_KEY --source https://push.chocolatey.org/
choco push choosenim.$env:CHOOSENIM_VERSION.nupkg --source https://push.chocolatey.org/

# Change location
Set-Location $currentLocation

# Move exe
Move-Item -Path ".\scripts\windows\choco\choosenim\choosenim.$env:CHOOSENIM_VERSION.nupkg" -Destination ".\upload-dir"